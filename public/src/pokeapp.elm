module Main exposing (main)

import Browser
import Html exposing (Html, button, div, text, section, a)
import Html.Events exposing (onClick)
import Html.Attributes exposing (class)
import Svg exposing (svg, image)
import Svg.Attributes exposing (viewBox, version)
import List
import Json.Decode exposing (Decoder)
import File
import Http
import Set
import Random
import Random.List exposing (choose)
import String.Extra
import Url
import Browser.Navigation as Nav

type alias Model 
    =
    {shapes : List(Polygon)
    ,zustand : Zustand
    ,aufgabe : Maybe Aufgabe
    ,images : List(String)
    ,currentImage : Maybe String
    ,score : (Int, Int)
    ,key : Nav.Key
    ,url : Url.Url
    }

type alias Polygon
     =
    {dexId : String 
    ,shiny : String 
    ,polypoints : String
    }

type Zustand 
    = Success
    | Failure

type Aufgabe 
    = Frage (Maybe Suche)
    | Information (Maybe PokeInfo)

type alias Suche 
    =
    {searchedPokeId : String 
    ,foundIt : Maybe Bool
    ,poke : Maybe Pokemon
    }

type alias Pokemon 
    =
    {pokedex : Int
    ,name : String
    ,types : List(String)
    ,spriteurl : String
    ,groesse : Int
    ,gewicht : Int
    --stats : List(String)
    --,texts : List(String)
    --,evolvesFrom : Maybe List(String)
    --,evolvesTo : Maybe List(String)
    }

type alias PokeInfo
    =
    {pokeId : String
    ,poke : Maybe Pokemon
    ,texts : Maybe (List(String))
    ,chosenText : Maybe String
    }

type Msg 
    = PolyClicked String String --erster String: Pokedex ID, zweiter String: Zusatzinfo
    | PokeGenerateClicked
    | PokeGenerated (Maybe String, List String)
    | TextChosen (Maybe String, List String)
    | GotPokemonInfo (Result Http.Error String)
    | GotSpeciesInfo (Result Http.Error String)
    | GotImageMap (Result Http.Error String) 
    | GotImageList (Result Http.Error String)
    | ChooseTaskButtonChanged String
    | ChooseImgButtonChanged String
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url

type alias FlavorText 
    =
    {flavorText : String
    ,languageName : String
    }


main : Program () Model Msg
main =
    Browser.application
        {init = initialModel
        ,view = view
        ,update = update
        ,subscriptions = subscriptions
        ,onUrlChange = UrlChanged
        ,onUrlRequest = LinkClicked
        }

initialModel : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
initialModel flags url key =
    let
        imgAufg =fragmentToImgAufgabe url.fragment
        befehle= 
            case imgAufg of
                Nothing ->
                    []
                Just (img, maufg) ->
                    case maufg of
                        Nothing ->
                            [Http.get
                                {url = relPath url "/src/Maps/" ++ (img) ++ ".json"
                                ,expect = Http.expectString GotImageMap
                                }
                            ]
                        Just aufg ->
                            case aufg of 
                                Frage Nothing ->
                                    [Http.get
                                        {url = relPath url "/src/Maps/" ++ (img) ++ ".json"
                                        ,expect = Http.expectString GotImageMap
                                        }
                                    ]
                                Frage (Just suche) ->
                                    [Http.get
                                        {url = relPath url "/src/Maps/" ++ (img) ++ ".json"
                                        ,expect = Http.expectString GotImageMap
                                        }
                                    ,Http.get
                                        {url = "https://pokeapi.co/api/v2/pokemon/"++suche.searchedPokeId
                                        ,expect = Http.expectString GotPokemonInfo
                                        }
                                    ]
                                Information Nothing ->
                                    [Http.get
                                        {url = relPath url "/src/Maps/" ++ (img) ++ ".json"
                                        ,expect = Http.expectString GotImageMap
                                        }
                                    ]
                                Information (Just pokeInfo) ->
                                    [Http.get
                                        {url = relPath url "/src/Maps/" ++ (img) ++ ".json"
                                        ,expect = Http.expectString GotImageMap
                                        }
                                    ,Http.get
                                        {url = "https://pokeapi.co/api/v2/pokemon/"++pokeInfo.pokeId
                                        ,expect = Http.expectString GotPokemonInfo
                                        }
                                    ]
        imgname = 
            case fragmentToImgAufgabe url.fragment of 
                Nothing ->
                    Nothing
                Just (img, maufg) ->
                    Just img                   
    in
        ({shapes = []
        ,zustand = Success
        ,aufgabe = aufgabeFromTuple (fragmentToImgAufgabe url.fragment)
        ,images = []
        ,currentImage = imgname
        ,score=(0,0)
        ,key = key
        ,url = url
        }
        ,Cmd.batch
            ([Http.get
                {url = relPath url "/src/ImageList.json"
                ,expect = Http.expectString GotImageList
                }
            ]
            ++befehle
            )        
        )
            
subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        PolyClicked id info->
            case model.aufgabe of
                Nothing ->
                    (model,Cmd.none)
                Just (Frage suche) -> 
                    case suche of
                        Nothing ->
                            (model, Cmd.none)
                        Just aktiveSuche ->
                            updatePolyClickedSuche model id aktiveSuche
                Just (Information mpokeinfo) -> 
                    let
                        oldurl=model.url
                        newpokeinfo = {pokeId = id , poke = Nothing, texts=Nothing, chosenText= Nothing}
                        newfragment=
                            case model.currentImage of 
                                Nothing ->
                                    Nothing
                                Just img ->
                                    (fragmentFromTuple  (Just (img, Just (Information (Just newpokeinfo)))))
                        newurl={oldurl|fragment=newfragment}
                    in
                        (model, Nav.pushUrl model.key (Url.toString newurl))
                  
                  
        PokeGenerateClicked ->
            (model, Random.generate PokeGenerated (zufallsID model))

        PokeGenerated (id,liste)->
            updatePokeGenerated model id

        GotPokemonInfo infojson ->
            updateGotPokeInfo model infojson

        GotSpeciesInfo infojson -> --TODO
            case infojson of
                Ok jsondatei -> 
                    
                    case model.aufgabe of
                            Just (Frage Nothing) ->
                                (model, Cmd.none)
                            Just (Frage (Just suche)) -> 
                                (model, Cmd.none)
                            Nothing ->
                                (model, Cmd.none)
                            Just (Information Nothing) ->
                                (model, Cmd.none)
                            Just (Information (Just pokeInfoAlt)) ->
                                let 
                                    poketexts = readPokeTexts jsondatei   
                                    newPokeInfo= {pokeInfoAlt|texts= Just poketexts}       
                                in 
                                    ({model|aufgabe = Just (Information (Just newPokeInfo))} 
                                    ,Random.generate TextChosen (zufallstext poketexts)
                                    )
                                            
                Err _ ->
                    ({model|shapes = [], zustand = Failure}, Cmd.none)
            

        GotImageMap res ->
            case res of
                Ok jsondatei ->    
                    ({model|shapes = readPolys jsondatei, zustand = Success}, Cmd.none)
                Err _ ->
                    ({model|shapes = [], zustand = Failure}, Cmd.none)

        GotImageList listjson ->    
            case listjson of
                Ok jsondatei -> 
                    let 
                        jsonliste =Json.Decode.decodeString (Json.Decode.list Json.Decode.string) jsondatei                       
                    in
                        case jsonliste of 
                            Ok liste ->                    
                                (
                                {model|images = liste}
                                ,Cmd.none
                                )
                            Err _ ->
                                ({model|shapes = [], zustand = Failure}, Cmd.none)
                Err _ ->
                    ({model|shapes = [], zustand = Failure}, Cmd.none)

        TextChosen (text, restliste) ->
            case text of 
                Just value -> 
                    case model.aufgabe of 
                        Just (Frage mSuche) ->
                            (model, Cmd.none)                      
                        Just (Information Nothing) ->
                            (model, Cmd.none)
                        Just (Information (Just pokeInfo)) ->
                            let 
                                newPokeInfo={pokeInfo|chosenText=text}
                            in
                                ({model| aufgabe= Just (Information (Just newPokeInfo))}, Cmd.none)
                        Nothing -> 
                            ({model|zustand = Failure}, Cmd.none)
                Nothing -> 
                            (model, Cmd.none)



        ChooseTaskButtonChanged str ->
            let
                oldurl = model.url        
            in
                case model.currentImage of
                    Just img ->
                        case str of 
                            "Frage" ->
                                (model, Nav.pushUrl model.key (Url.toString {oldurl|fragment = Just (img ++ "-search")}))
                            "Info" ->
                                (model, Nav.pushUrl model.key (Url.toString {oldurl|fragment = Just (img ++ "-info")}))
                            "" ->
                                (model, Nav.pushUrl model.key (Url.toString {oldurl|fragment = Nothing})) 
                            _ -> 
                                (model, Cmd.none)
                    Nothing ->
                        ({model|zustand=Failure}, Cmd.none)
        
        ChooseImgButtonChanged img ->
            let
                oldurl=model.url
                newfragment = fragmentFromTuple (Just (img, Nothing))
                newurl={oldurl|fragment=newfragment}
            in
            (
                {model|currentImage = Just img}
                ,Nav.pushUrl model.key (Url.toString newurl)
            )

        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    (model, Nav.pushUrl model.key (Url.toString url))

                Browser.External href ->
                    (model, Nav.load href)

        UrlChanged url ->   
            case fragmentToImgAufgabe url.fragment of
                Nothing ->
                    ({model|aufgabe = Nothing, currentImage=Nothing, shapes = [], zustand=Success}, Cmd.none)
                Just (img, maufg) ->
                    case maufg of 
                        Just (Frage msuche) ->
                            case msuche of 
                                Nothing ->
                                    ({model|aufgabe = Just (Frage Nothing)}, Cmd.none)
                                Just suche ->
                                    ({model|aufgabe = Just (Frage (Just suche))}
                                    , Cmd.batch 
                                        [
                                        Http.get
                                            {url = "https://pokeapi.co/api/v2/pokemon/"++suche.searchedPokeId
                                            ,expect = Http.expectString GotPokemonInfo
                                           }
                                        ,Http.get
                                            {url = relPath model.url ("/src/Maps/"++ img ++".json")
                                            ,expect = Http.expectString GotImageMap
                                            }
                                        ]

                                    )            
                        Just (Information mpokeInfo) ->
                            case mpokeInfo of
                                Nothing ->
                                    ({model|aufgabe = Just (Information Nothing)}, Cmd.none)
                                Just pokeInfo ->
                                    ({model|aufgabe = Just (Information (Just pokeInfo))}
                                    , 
                                    Cmd.batch 
                                        [   Http.get
                                                {url = "https://pokeapi.co/api/v2/pokemon/"++pokeInfo.pokeId
                                                ,expect = Http.expectString GotPokemonInfo
                                                }
                                            ,Http.get
                                                {url = relPath model.url ("/src/Maps/"++ img ++".json")
                                                ,expect = Http.expectString GotImageMap
                                                }
                                        ]
                                    )
                            
                        Nothing ->
                            ({model|aufgabe = Nothing} 
                            ,Http.get
                                {url = relPath model.url ("/src/Maps/"++ img ++".json")
                                ,expect = Http.expectString GotImageMap
                                }
                            )

updatePokeGenerated : Model -> Maybe String -> (Model, Cmd Msg)
updatePokeGenerated model id=
    case id of 
        Just value -> 
            let 
                aufg =  (Just (Frage (Just {searchedPokeId=value, foundIt=Nothing, poke=Nothing})))
                newfragment = 
                    case model.currentImage of 
                        Nothing ->    
                            Nothing
                        Just img ->
                            fragmentFromTuple (Just (img, aufg))
                oldurl=model.url
                newurl = {oldurl|fragment = newfragment}
            in
                (model, Nav.pushUrl  model.key (Url.toString newurl))
        Nothing -> 
            ({model|zustand = Failure}, Cmd.none)

updateGotPokeInfo: Model -> Result Http.Error String ->(Model, Cmd Msg)
updateGotPokeInfo model infojson=
    case infojson of
        Ok jsondatei -> 
            case model.aufgabe of
                    Just (Frage Nothing) ->
                        ({model|zustand = Failure}, Cmd.none)
                    Just (Frage (Just suche)) -> 
                        let 
                            oldaktiveSuche = suche
                            poke = Json.Decode.decodeString readSinglePokemon jsondatei
                            newaktiveSuche = 
                                case poke of
                                    Ok pokemon ->
                                        {oldaktiveSuche | poke = Just pokemon}
                                    Err error ->
                                        oldaktiveSuche
                        in 
                            case poke of 
                                Ok wert ->
                                    ({model|aufgabe = Just (Frage (Just newaktiveSuche))} ,Cmd.none)
                                Err error ->
                                    ({model|zustand = Failure}, Cmd.none)
                    Nothing ->
                        (model, Cmd.none)
                    Just (Information pokeInfoAlt) ->
                         let 
                            poke = Json.Decode.decodeString readSinglePokemon jsondatei            
                        in 
                            case poke of
                                    Ok pokemon ->
                                        let
                                            newPokeInfo = 
                                                {pokeId = String.fromInt pokemon.pokedex
                                                ,poke = Just pokemon
                                                ,texts = Nothing
                                                ,chosenText = Nothing
                                                }    
                                        in
                                            ({model|aufgabe = Just (Information (Just newPokeInfo))} 
                                            ,Http.get
                                                {url = "https://pokeapi.co/api/v2/pokemon-species/"++(String.fromInt pokemon.pokedex)
                                                ,expect = Http.expectString GotSpeciesInfo
                                                }
                                            )
                                    Err error ->
                                        ({model|zustand = Failure}, Cmd.none)
        Err _ ->
            ({model|shapes = [], zustand = Failure}, Cmd.none)

updatePolyClickedSuche: Model -> String -> Suche -> (Model, Cmd Msg)
updatePolyClickedSuche model id suche =
    if id ==suche.searchedPokeId then
        let 
            newaktiveSuche = {suche | foundIt=Just True}
            newscore=((Tuple.first model.score)+1, Tuple.second model.score)
        in           
            ({model|aufgabe = Just (Frage (Just newaktiveSuche)), score=newscore} ,Cmd.none)
    else
        let 
            newaktiveSuche = {suche | foundIt=Just False}
            newscore=(Tuple.first model.score, (Tuple.second model.score)+1)
        in           
            ({model|aufgabe = Just (Frage (Just newaktiveSuche)), score=newscore} ,Cmd.none)

view : Model -> Browser.Document Msg
view model =
    {title =
        "Pokemon App"
    ,body =
        [div [class "inhalt"]
            [buttons model
            ,ashCatchThem (ashsText model)
            ,clickableImage model
            ]
        ]
    }

buttons : Model -> Html Msg
buttons model =
    div [class "nes-container is-rounded inhaltsElemente"]
        ([imgDropdownButton model, taskDropdownButton model, searchButtons model])

searchButtons : Model -> Html Msg
searchButtons model =
    div[class "flex-container"]
        ((pokegenerateButton model)++(displayScore model))

pokegenerateButton : Model -> List(Html Msg)
pokegenerateButton model =
    let 
        link = 
            case model.currentImage of
                Nothing ->
                    ""
                Just img ->
                    "#" ++ img ++ "-" ++ "search"
    in
    case model.aufgabe of 
        Just (Frage msuche) ->
            case msuche of 
                Nothing ->
                    [a  [class "nes-btn distance generate flex-item", onClick PokeGenerateClicked, Html.Attributes.href link] 
                        [text "Ask me a question!"]
                    ]
                Just suche ->
                    [a  [class "nes-btn distance generate flex-item", onClick PokeGenerateClicked, Html.Attributes.href link] 
                        [text "Ask me another question!"]
                    ]
        Just (Information poke) ->
            []
        
        Nothing ->
            []

displayScore : Model -> List(Html Msg)
displayScore model =
    let
        score=
            Html.div [class "nes-container with-title is-centered is-rounded score flex-item"]
            [
                Html.p [class "title"][text "Score"]
                ,Html.div [class "lists"]
                    [Html.ul [class "nes-list is-disc"]
                        [Html.li [] [text ("hits: \t" ++ (String.fromInt (Tuple.first model.score)))]
                        ,Html.li [] [text ("misses: \t" ++ (String.fromInt (Tuple.second model.score)))]
                        ]  
                    ]
            ]
    in
        case model.aufgabe of 
            Just (Frage msuche) ->
                [score]
            Just (Information poke) ->
                []
            Nothing ->
                []  

taskDropdownButton : Model -> Html Msg
taskDropdownButton model =
    let
        options = 
            case model.aufgabe of
                Just (Information poke) ->         
                    [Html.option 
                        [Html.Attributes.value ""
                        ,Html.Attributes.disabled True
                        ,Html.Attributes.hidden True
                        ] 
                        [text "What should I do?"]
                    ,Html.option [Html.Attributes.value "Frage"] [text "Ask me something about Pokemon."]
                    ,Html.option [Html.Attributes.value "Info", Html.Attributes.selected True] [text "Tell me something about Pokemon."]
                    ]
                Just (Frage search) ->
                    [Html.option 
                        [Html.Attributes.value ""
                        ,Html.Attributes.disabled True
                        ,Html.Attributes.hidden True
                        ] 
                        [text "What should I do?"]
                    ,Html.option [Html.Attributes.value "Frage", Html.Attributes.selected True] [text "Ask me something about Pokemon."]
                    ,Html.option [Html.Attributes.value "Info"] [text "Tell me something about Pokemon."]
                    ]
                Nothing ->
                    [Html.option 
                        [Html.Attributes.value ""
                        ,Html.Attributes.disabled True
                        ,Html.Attributes.selected True
                        ,Html.Attributes.hidden True
                        ] 
                        [text "What should I do?"]
                    ,Html.option [Html.Attributes.value "Frage"] [text "Ask me something about Pokemon."]
                    ,Html.option [Html.Attributes.value "Info"] [text "Tell me something about Pokemon."]
                    ]
    in
        case model.currentImage of 
            Just str ->
                div [class "nes-select distance"] 
                    [Html.select [Html.Attributes.id "Tasks select.", Html.Events.onInput ChooseTaskButtonChanged] 
                        options
                    ]
            Nothing ->
                div[][]

imgDropdownButton : Model -> Html Msg
imgDropdownButton model =
    let
        options = 
            (
                [Html.option 
                    [Html.Attributes.value ""
                    ,Html.Attributes.disabled True
                    ,Html.Attributes.selected True
                    ,Html.Attributes.hidden True
                    ] 
                    [text "Choose an image."]
                ]
                ++ 
                imageDropdownItems model
            )
    in
    div [class "nes-select"] 
        [Html.select [Html.Attributes.id "image-select", Html.Events.onInput ChooseImgButtonChanged] 
            options
        ]

imageDropdownItems : Model -> List(Html Msg)
imageDropdownItems model =
    List.map (singleDropdownItem model.currentImage) model.images   
    
singleDropdownItem : Maybe String -> String -> Html Msg
singleDropdownItem mcurr item =
    case mcurr of 
        Just curr ->
            if curr==item then 
                Html.option [Html.Attributes.value item, Html.Attributes.selected True] [text item]
            else
                Html.option [Html.Attributes.value item] [text item]
        Nothing ->  
            Html.option [Html.Attributes.value item] [text item]


ashCatchThem : Html Msg -> Html Msg
ashCatchThem ashstext =
    section [class "message-list message-listPlus"]  
        [section [class "message -left messagePlus"]
            [Html.i [class "nes-ash ash"] []
            ,div    [class "nes-balloon from-left nes-balloonPlus"] 
                    [Html.p [] [ashstext]
                    ]
            ]
        ]

ashsText : Model ->  Html Msg
ashsText model =
    case model.zustand of
        Failure ->
            text "Oh, seems like an error happened somewhere."
        Success -> 
            case model.aufgabe of
                Nothing -> 
                    text "Hello, I'm Ash. If you want, I'll quiz you on your pokemon knowledge or tell you something about the Pokemon first. Just select what you want me to do."
                Just (Frage (Just suche)) ->
                    case suche.foundIt of
                        Nothing -> 
                            case suche.poke of
                                Just pokemon -> 
                                    text ("Search for "++(pokemon.name)++" in this picture.")
                                Nothing ->
                                    text "Hmmm..."
                        Just True ->
                            text "Great, that's right!"
                        Just False ->
                            case suche.poke of
                                Just pokemon ->
                                    text ("Oh, that's not "++(pokemon.name)++ ", but you can try again.")
                                Nothing ->
                                    text "Hmmm..."
                Just (Information mPokeInfo) -> 
                    case mPokeInfo of 
                        Nothing -> 
                            text "Just click on the Pokemon you want to know something about."
                        Just pokeInfo ->
                            case pokeInfo.poke of 
                                Just poke ->
                                    div [] 
                                        [div [class "text"] 
                                            [pokeToString poke pokeInfo.chosenText]
                                        ,div [class "sprite"] 
                                            [Html.img [Html.Attributes.src poke.spriteurl] []]
                                        ] 
                                Nothing ->
                                    text "Hmmm..."
                Just (Frage Nothing) ->
                    text "Just click on the button above and I'll ask you a question."
                   
pokeToString : Pokemon -> Maybe String-> Html Msg
pokeToString poke mFlavortext =
    let
        flavortext =
            case mFlavortext of
                Nothing -> 
                    ""
                Just jflavortext ->
                    String.replace "\n" " " jflavortext
    in
        div[]
        [
            Html.p []
                [text  ("That's " ++ poke.name ++ ". It has the id " ++ (String.fromInt poke.pokedex) ++". ")
                ,text ((String.Extra.toSentenceCase poke.name) ++ (poketypentoString poke.types))
                ,text ("It's " ++ String.fromInt (poke.groesse*10) ++ " cm tall and weights " ++ String.fromFloat (toFloat poke.gewicht/10.0) ++ "kg. ")
                ]
            ,Html.p []
                [text ("\r" ++ flavortext)]
        ]

poketypentoString : List(String) -> String
poketypentoString list =
    case list of 
        [] ->
            "has apparently no types. I think something went wrong here. "
        poketype::[] -> 
            "'s type is " ++ poketype ++ ". "
        poketype::typelist -> 
            "'s types are " ++ poketype ++ (List.foldr (++) "" (List.map (\a -> " and " ++ a) typelist)) ++ ". "



--erstellt das Bild und die Polygone   
clickableImage : Model -> Html Msg
clickableImage model = 
    case model.currentImage of
        Nothing ->
            section [class "section inhaltsElemente"][]
        Just curr ->
            section [class "section inhaltsElemente"]
                [div [class "container"]
                    [Html.figure [ class "image" ]
                        [svg [Svg.Attributes.class "viewBoxCenter", Svg.Attributes.width "100%", viewBox "0 0 1920 1080", version "1.1"]
                            ([Svg.defs[]
                                [Svg.style[]
                                    --css für mittige viewBox
                                    [text """   .viewBoxCenter {
                                                            display: block;
                                                            margin-left: auto;
                                                            margin-right: auto;
                                                            }
                                                            
                                                .polygon { 
                                                            fill: black;   
                                                            fill-opacity:0;
                                                        }
                                                .polygon:hover {
                                                            fill: white;
                                                            fill-opacity:0.03;
                                                }"""
                                    ]        
                                ]
                            ,image [ Html.Attributes.width 1920, Html.Attributes.height 1080, Html.Attributes.title curr, Svg.Attributes.xlinkHref (relPath model.url ("/src/Bilder/"++curr++".png")) ] []
                            ]
                            ++(List.map svgPoly model.shapes)
                            )
                        ]
                    ]
                ]


-- Decoder fuer json-Polygone zu Polygonen
readSinglePoly : Decoder Polygon
readSinglePoly =
    Json.Decode.map3 Polygon
        (Json.Decode.field "Pokedex No" Json.Decode.string)
        (Json.Decode.field "Zusatzinfo" Json.Decode.string)
        (Json.Decode.field "Polygon points" Json.Decode.string)

-- bekommt String (json Liste von  Polygonen) uebergeben, erstellt Liste von Polygonen
readPolys : String -> List(Polygon)
readPolys str = 
    let result = Json.Decode.decodeString (Json.Decode.list readSinglePoly) str
    in 
        case result of
            Ok wert ->
                wert
            Err error ->
                []

--erstellt aus einem Polygon eine Html Msg mit einem svg polygon
svgPoly : Polygon ->Svg.Svg Msg
svgPoly poly= 
    Svg.polygon[Svg.Attributes.points poly.polypoints, Svg.Attributes.class "polygon" , onClick (PolyClicked poly.dexId poly.shiny) ][]


getID : Polygon -> String
getID poly =
    poly.dexId

zufallsID : Model -> Random.Generator (Maybe String, List String)
zufallsID model =
    (choose (Set.toList (Set.fromList (List.map getID model.shapes))))
 
zufallstext : List(String) -> Random.Generator (Maybe String, List String)
zufallstext texte =
    choose (Set.toList (Set.fromList texte))

readPokemons : String -> List(Pokemon)
readPokemons str =
    let 
        result = Json.Decode.decodeString (Json.Decode.list readSinglePokemon) str
    in 
        case result of
            Ok wert ->
                wert
            Err error ->
                []

getPokeName : String -> String
getPokeName str = 
    let result = Json.Decode.decodeString (Json.Decode.field "name" Json.Decode.string) str
    in 
        case result of
            Ok wert ->
                wert
            Err error ->
                ""
    
-- Decoder fuer json-Pokemon zu Pokemon
readSinglePokemon : Decoder Pokemon
readSinglePokemon =
    Json.Decode.map6 Pokemon
        (Json.Decode.field "id" Json.Decode.int)
        (Json.Decode.field "name" Json.Decode.string)
        (Json.Decode.field "types" (Json.Decode.list (Json.Decode.field "type" (Json.Decode.field "name" Json.Decode.string))))
        (Json.Decode.at ["sprites", "front_default"] Json.Decode.string)
        (Json.Decode.field "height" Json.Decode.int)
        (Json.Decode.field "weight" Json.Decode.int)

readTypes : Decoder String
readTypes =
    (Json.Decode.field "type" (Json.Decode.field "name" Json.Decode.string))           

-- bekommt String (json Liste von  Pokemon) uebergeben, erstellt Liste von Pokemon
readPokeTexts : String -> List(String)
readPokeTexts str = 
    let 
        result = Json.Decode.decodeString (Json.Decode.at ["flavor_text_entries"] (Json.Decode.list readSingleText)) str
    in 
        case result of
            Ok wert ->
                List.map (\flavor -> flavor.flavorText) (List.filter (\flavor -> flavor.languageName=="en") wert)
            Err error ->
                []

readSingleText : Decoder (FlavorText)
readSingleText =
    Json.Decode.map2 FlavorText
        (Json.Decode.at ["flavor_text"] Json.Decode.string )
        (Json.Decode.at ["language", "name"] Json.Decode.string)
        


relPath : Url.Url -> String -> String
relPath url str =
    let
        newUrl = 
            {protocol = url.protocol
            ,host = url.host
            ,port_ = url.port_
            ,path = (String.replace "/index.html" "" url.path) ++ str
            ,query = Nothing
            ,fragment = Nothing 
            }  
    in
        Url.toString (newUrl)

aufgabeToFragmentString : Maybe Aufgabe -> String
aufgabeToFragmentString auf = 
    case auf of 
        Nothing ->
            ""
        Just (Frage msuche) -> 
            case msuche of 
                Nothing ->
                    ("-search")
                Just suche ->
                    ("-search-"++suche.searchedPokeId)    
        Just (Information mPokeInfo) ->
            case mPokeInfo of
                Nothing ->
                    "-info"
                Just pokeInfo ->
                    ("-info-"++ (pokeInfo.pokeId))

fragmentToImgAufgabe : Maybe String -> Maybe (String, Maybe Aufgabe)
fragmentToImgAufgabe mstr =
    case mstr of 
        Nothing ->
            Nothing
        Just str ->
            let 
                list = String.split "-" str 
            in
                case list of 
                    [] ->
                        Nothing
                    pic::[] ->
                        Just (pic, Nothing)
                    pic::rest ->  
                        Just (pic, fragmentToAufgabe rest)

fragmentToAufgabe : List(String) -> Maybe Aufgabe
fragmentToAufgabe liste = 
    case liste of 
        [] ->
            Nothing
        task::[] ->
            if (task == "info") then 
                Just (Information Nothing)
            else if (task == "search") then 
                Just (Frage Nothing)
            else 
                Nothing
        task::pokeid::[] ->
            if (task == "info") then
                (let 
                    newPokeInfo = 
                        {pokeId = String.filter Char.isDigit (pokeid)
                        ,poke = Nothing
                        ,texts = Nothing
                        ,chosenText = Nothing
                        }
                in
                    Just (Information (Just newPokeInfo)) 
                )
            else if (task == "search") then 
                (let 
                    newSuche = 
                        {searchedPokeId = String.filter Char.isDigit pokeid
                        ,poke = Nothing
                        ,foundIt= Nothing
                        }
                in
                    Just (Frage (Just newSuche))
                )
            else 
                Nothing
        _ ->
            Nothing


aufgabeFromTuple : Maybe (String, Maybe Aufgabe) -> Maybe Aufgabe
aufgabeFromTuple mtuple = 
    case mtuple of 
        Nothing ->
            Nothing
        Just tuple ->
            case Tuple.second tuple of 
                Nothing ->
                    Nothing
                Just aufgabe ->
                    Just aufgabe
    
fragmentFromTuple : Maybe (String, Maybe Aufgabe) -> Maybe String
fragmentFromTuple mtuple =
    case mtuple of
        Just (img, maufg) ->
            Just( img ++ (aufgabeToFragmentString maufg))
        Nothing ->
            Nothing

makePokeInfoFromId : Int -> PokeInfo
makePokeInfoFromId id = 
    {pokeId = String.fromInt id
    ,poke = Nothing
    ,texts = Nothing
    ,chosenText = Nothing
    }